# Prérequis

- [ ] Installer et charger le package RPostgres
- [ ] Renommer le script template_connexion.R et en connexion.R et le compléter avec les informations de connexions à la base geriatrie.
- [ ] S'assurer que "connexion.R" est renseigné dans le .gitignore.
- [ ] Tester la connexion à la base de données dans script.R. 

# Data management

- Dans la table rum, ajouter un indice correspondant au rang du RUM dans le séjour

- Pour chaque rum, ajouter un rum fictif de rang 0 avec le mode d'entrée du premier RUM

- Pour chaque rum, ajouter un rum fictif de rang r + 1 (r étant le rang max de chacun des séjour) avec le mode sortie

# Analyses

- Décrire la population (sexe, âge à l'admission, durée de séjour, mortalité) avec les indicateurs statistiques et/ou les graphiques pertinents. 

- Représenter sous forme graphique l'effectif de chacun des modes d'entrée

- Représenter sous forme graphique l'effectif de chacune des UFs (pour tous les RUMs)

- Représenter sous forme graphique l'effectif des UFs des premiers RUMs

- Représenter sous forme graphique l'effectif de chacun des modes de sortie

- Compter les associations d'UFs pour les deux premiers RUMs.

- Compter les associations d'UFs pour toutes les transitions entre RUMs.

-  Compter les transitions du mode d'entrée vers le premier RUM.

- Compter les transitions du dernier rum vers le mode de sortie.

- Compter les séquences complètes (ex : Urgences - UF1 - UF2 - Domicile = 5 séjours).

# Présentation des résultats

- Présenter les résultats sous forme de Rmarkdown.
